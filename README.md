## Import Status

### idr-testing (if applicable)
| Task | Duration | Checked |
| :----: |:----:| :----:|
| Images| 1 hour | X |
| Thumbnails | 1 hour | X |
| Annotations | 10 min | X |

### idr-next
| Task | Duration | Checked |
| :----: |:----:| :----:|
| Images| -- | -- |
| Thumbnails | -- | -- |
| Annotations | -- | -- |
